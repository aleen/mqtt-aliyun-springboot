package com.gitee.aleenjava.mqtt.consume;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cheng
 */
@SpringBootApplication
@Slf4j
public class ConsumeApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumeApplication.class.getName());
    public static void main(String[] args) {
        SpringApplication.run(ConsumeApplication.class, args);
    }

}
