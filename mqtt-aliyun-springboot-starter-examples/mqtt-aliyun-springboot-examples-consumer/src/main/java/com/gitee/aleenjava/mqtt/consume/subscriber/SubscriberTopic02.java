package com.gitee.aleenjava.mqtt.consume.subscriber;

import com.gitee.aleenjava.aliyun.mqtt.annotation.MqttSubscriber;
import com.gitee.aleenjava.aliyun.mqtt.processor.MqttSubscribeProcessor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * @author cheng
 */
@Slf4j
@MqttSubscriber(topic = "topic-test02")
public class SubscriberTopic02 implements MqttSubscribeProcessor {
    @Override
    public void process(String topic, MqttMessage message) {
        log.info("topic-test02:{}, message:{}", topic, new String(message.getPayload()));
    }
}
