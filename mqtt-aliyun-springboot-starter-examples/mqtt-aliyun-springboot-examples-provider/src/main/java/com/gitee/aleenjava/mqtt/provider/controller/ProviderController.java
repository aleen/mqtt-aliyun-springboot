package com.gitee.aleenjava.mqtt.provider.controller;


import com.gitee.aleenjava.aliyun.mqtt.processor.MqttPublishProcessor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Random;
import java.util.UUID;


/**
 * @author cheng
 */
@RestController
@RequestMapping("/mqtt")
public class ProviderController {
    @Resource
    MqttPublishProcessor mqttPublishProcessor;

    @RequestMapping("/push")
    public ResponseEntity<String> push(@RequestParam(name = "topic") String topic) throws Exception {
        mqttPublishProcessor.publish(topic, topic + "@@@message@@@" + UUID.randomUUID().toString());
        return ResponseEntity.ok("success");
    }

}
