package com.gitee.aleenjava.aliyun.mqtt.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * @author cheng
 */
@Data
@ConfigurationProperties(prefix = "mqtt.publisher")
public class MqttPublisherProperties {
    /**
     * 是否开启mqtt客户端
     */
    private boolean enable = false;
    /**
     * mqtt broker ip
     */
    private String ip;
    /**
     * mqtt broker 端口
     */
    private String port;

    /**
     * MQ4IOT 实例 ID，购买后控制台获取
     */
    private String instanceId;
    /**
     * 账号 accesskey，从账号系统控制台获取
     */
    private String accessKey;

    /**
     * groupIds从账号系统控制台获取获取
     */
    private List<String>  groupIds;

    /**
     * topics
     */
    private List<String> topics;

    /**
     * 账号 secretKey，从账号系统控制台获取，仅在Signature鉴权模式下需要设置
     */
    private String secretKey;
    /**
     * mqtt 客户端id是否随机生成
     */
    private boolean enableRandomClientId = true;
    /**
     * mqtt 客户端id
     */
    private List<String> clientIds;

    /**
     * mqtt 客户端id 默认生成数量
     */
    private int clientIdNums = 1;


    /**
     * 连接超时时间(秒)
     */
    private int connectionTimeout = 5;

    /**
     * 默认Publisher Qos [0,1,2]
     */
    private int qos = 2;

    /**
     * 发布消息从连接池获取链接超时时间(毫秒)
     */
    private long connectPoolPollTimeout = 3000L;


    @PostConstruct
    private void init() {
        if (enable) {
            String prefix = this.getClass().getAnnotation(ConfigurationProperties.class).prefix();
            Assert.hasText(ip, "No mqtt broker IP configured in publisher {" + prefix + ".ip}");
            Assert.hasText(port, "No mqtt broker port configured in publisher : {" + prefix + ".port}");
        }
    }
}
