package com.gitee.aleenjava.aliyun.mqtt.model;

import com.gitee.aleenjava.aliyun.mqtt.enums.QosEnum;
import com.gitee.aleenjava.aliyun.mqtt.processor.MqttSubscribeProcessor;
import lombok.Builder;
import lombok.Data;

/**
 * 订阅主题信息
 * @author cheng
 */
@Data
@Builder
public class SubscriberInfo {
    /**
     * 主题名称
     */
    private String topic;
    /**
     * 传输质量
     */
    private QosEnum qos;
    /**
     * 订阅主题处理器
     */
    private MqttSubscribeProcessor mqttSubscribeProcessor;

}
