package com.gitee.aleenjava.aliyun.mqtt.config;

import com.gitee.aleenjava.aliyun.mqtt.model.ApplicationContextHelper;
import com.gitee.aleenjava.aliyun.mqtt.register.MqttSubscriberRegister;
import com.gitee.aleenjava.aliyun.mqtt.service.subscribe.MqttSubscribeClientService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author cheng
 */
@Configuration
@ConditionalOnProperty(prefix = "mqtt.subscriber", name = "enable", havingValue = "true")
public class MqttSubscriberConfiguration {

    @Bean
    public MqttSubscriberProperties mqttSubscriberProperties() {
        return new MqttSubscriberProperties();
    }

    @Bean
    public MqttSubscriberRegister mqttSubscriberRegister() {
        return new MqttSubscriberRegister();
    }

    @Bean(initMethod = "init")
    public MqttSubscribeClientService mqttSubscribeClientService() {
        return new MqttSubscribeClientService(mqttSubscriberProperties(), mqttSubscriberRegister());
    }
}
