package com.gitee.aleenjava.aliyun.mqtt.config;

import com.gitee.aleenjava.aliyun.mqtt.processor.MqttPublishProcessor;
import com.gitee.aleenjava.aliyun.mqtt.service.client.MqttPublishClient;
import com.gitee.aleenjava.aliyun.mqtt.service.client.MqttPublishProcessorImpl;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author cheng
 */
@Configuration
@ConditionalOnProperty(prefix = "mqtt.publisher", name = "enable", havingValue = "true")
public class MqttPublisherConfiguration {
    @Autowired
    MqttPublisherProperties mqttPublisherProperties;

    @Bean
    public MqttPublishClient mqttPublishClient() throws MqttException {
        return new MqttPublishClient(mqttPublisherProperties);
    }

    @Bean
    public MqttPublishProcessor mqttPublishProcessor() throws MqttException {
        return new MqttPublishProcessorImpl(mqttPublisherProperties, mqttPublishClient());
    }
}