package com.gitee.aleenjava.aliyun.mqtt.annotation;

import com.gitee.aleenjava.aliyun.mqtt.enums.QosEnum;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;


/**
 * 订阅主题
 *
 * @author cheng
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface MqttSubscriber {

    String MQTT_TOPIC_PLACEHOLDER = "${mqtt.subscriber.topic:}";

    /**
     * 订阅topic
     *
     * @return
     */
    String topic() default  MQTT_TOPIC_PLACEHOLDER;

    /**
     * qos
     *
     * @return
     */
    QosEnum qos() default QosEnum.Q_2_EXACTLY_ONCE;

}
