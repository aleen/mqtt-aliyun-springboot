package com.gitee.aleenjava.aliyun.mqtt;


import com.gitee.aleenjava.aliyun.mqtt.config.MqttPublisherConfiguration;
import com.gitee.aleenjava.aliyun.mqtt.config.MqttPublisherProperties;
import com.gitee.aleenjava.aliyun.mqtt.config.MqttSubscriberConfiguration;
import com.gitee.aleenjava.aliyun.mqtt.config.MqttSubscriberProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


/**
 * @author cheng
 */
@Configuration
@ConditionalOnWebApplication
@EnableConfigurationProperties({MqttPublisherProperties.class, MqttSubscriberProperties.class})
@Import({MqttPublisherConfiguration.class, MqttSubscriberConfiguration.class})
public class MqttAliYunAutoConfigure {

}
