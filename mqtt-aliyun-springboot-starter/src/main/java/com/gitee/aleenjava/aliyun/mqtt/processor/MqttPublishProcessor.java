package com.gitee.aleenjava.aliyun.mqtt.processor;


import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * 消息发布处理器
 * @author cheng
 */
public interface MqttPublishProcessor {
    /**
     * 发送消息
     *
     * @param topic
     * @param message
     */
    void publish(String topic, MqttMessage message) throws MqttException;

    /**
     * 发送消息
     *
     * @param topic
     * @param message
     * @throws Exception
     */
    void publish(String topic, String message) throws MqttException;

    /**
     * 发送消息
     *
     * @param topic
     * @param message
     * @throws Exception
     */
    void publish(String topic, byte[] message) throws MqttException;
}
